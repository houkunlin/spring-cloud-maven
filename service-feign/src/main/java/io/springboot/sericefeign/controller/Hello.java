package io.springboot.sericefeign.controller;

import io.springboot.sericefeign.service.IHelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello {

    @Autowired
    IHelloService helloService;

    @RequestMapping("/hello")
    public String hello(String params) {
        return helloService.hello(params);
    }
}
