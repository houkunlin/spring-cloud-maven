package io.springboot.sericefeign.service;

import io.springboot.sericefeign.service.impl.HelloServerImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author 侯坤林
 */
@FeignClient(value = "ibona-form", fallback = HelloServerImpl.class)
public interface IHelloService {
    /**
     * 访问提供表单功能的微服务。
     * 获取表单列表
     *
     * @param limit 分页每页条数
     * @return 返回字符串
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    String hello(@RequestParam(value = "limit") String limit);
}
