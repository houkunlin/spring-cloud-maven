package cn.goour.service;

import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormInfoDO;
import com.github.pagehelper.PageInfo;
import io.springboot.dto.PageInfoDTO;

/**
 * 表单业务处理接口
 *
 * @author 侯坤林
 */
public interface FormInfoService {
    /**
     * 查询表单列表
     *
     * @param searchInfoDTO 查询条件
     * @param pageInfoDTO   分页条件
     * @return 分页列表
     */
    PageInfo<FormInfoDO> list(SearchInfoDTO searchInfoDTO, PageInfoDTO pageInfoDTO);

    /**
     * 添加表单信息
     *
     * @param formInfoDO 表单信息
     */
    void insert(FormInfoDO formInfoDO);

    /**
     * 修改表单信息
     *
     * @param formInfoDO 表单信息
     */
    void updateById(FormInfoDO formInfoDO);

    /**
     * 删除表单信息
     *
     * @param formInfoId 表单ID
     */
    void deleteById(Integer formInfoId);

    /**
     * 设置表单是否启用
     *
     * @param formInfoId 表单ID
     * @param isEnabled  是否启用
     */
    void setEnabled(Integer formInfoId, Boolean isEnabled);

    /**
     * 通过ID查询一条记录
     *
     * @param formInfoId 表单ID
     * @return 表单
     */
    FormInfoDO getById(Integer formInfoId);
}
