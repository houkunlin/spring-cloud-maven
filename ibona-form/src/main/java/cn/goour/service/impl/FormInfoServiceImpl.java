package cn.goour.service.impl;

import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormInfoDO;
import cn.goour.mapper.FormFieldMapper;
import cn.goour.mapper.FormInfoMapper;
import cn.goour.service.FormInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.springboot.dto.PageInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 表单信息业务逻辑处理
 *
 * @author 侯坤林
 */
@Service
public class FormInfoServiceImpl implements FormInfoService {

    @Autowired
    FormInfoMapper formInfoMapper;

    @Autowired
    FormFieldMapper formFieldMapper;

    @Override
    public PageInfo<FormInfoDO> list(SearchInfoDTO searchInfoDTO, PageInfoDTO pageInfoDTO) {
        PageInfo<FormInfoDO> pageInfo = PageHelper.startPage(pageInfoDTO.getPage(), pageInfoDTO.getLimit()).doSelectPageInfo(() -> {
            formInfoMapper.listFormInfo(searchInfoDTO);
        });
        return pageInfo;
    }

    @Override
    public void insert(FormInfoDO formInfoDO) {
        formInfoMapper.insert(formInfoDO);
    }

    @Override
    public void updateById(FormInfoDO formInfoDO) {
        formInfoMapper.updateById(formInfoDO);
    }

    @Override
    public void deleteById(Integer formInfoId) {
        // 删除表单
        formInfoMapper.deleteById(formInfoId);
        // 删除表单ID会同时删除该表单下面的字段
        formFieldMapper.deleteByFormId(formInfoId);
    }

    @Override
    public void setEnabled(Integer formInfoId, Boolean isEnabled) {
        formInfoMapper.setEnabled(formInfoId, isEnabled);
    }

    @Override
    public FormInfoDO getById(Integer formInfoId) {
        return formInfoMapper.getById(formInfoId);
    }
}
