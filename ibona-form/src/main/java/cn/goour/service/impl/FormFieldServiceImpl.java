package cn.goour.service.impl;

import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormFieldDO;
import cn.goour.entity.FormInfoDO;
import cn.goour.mapper.FormFieldMapper;
import cn.goour.mapper.FormInfoMapper;
import cn.goour.service.FormFieldService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.springboot.dto.PageInfoDTO;
import io.springboot.exception.BackJsonException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 表单字段信息业务处理
 *
 * @author 侯坤林
 */
@Service
public class FormFieldServiceImpl implements FormFieldService {
    @Autowired
    FormFieldMapper formFieldMapper;

    @Autowired
    FormInfoMapper formInfoMapper;

    @Override
    public PageInfo<FormFieldDO> list(SearchInfoDTO searchInfoDTO, PageInfoDTO pageInfoDTO, Integer formInfoId) {
        return PageHelper.startPage(pageInfoDTO.getPage(), pageInfoDTO.getLimit())
                .doSelectPageInfo(() -> formFieldMapper.listFormField(searchInfoDTO, formInfoId));
    }

    @Override
    public void updateByIdAndFormInfoId(FormFieldDO formFieldDO) {
        FormInfoDO info1 = formFieldMapper.getFormInfoByFormFieldId(formFieldDO.getId());
        if (info1 == null) {
            // 无法找到与当前字段信息关联的表单信息
            // 因此需要删除当前字段信息
            formFieldMapper.deleteById(formFieldDO.getId());
            throw new BackJsonException("当前表单丢失，您已无法修改该表单的字段");
        }
        formFieldMapper.updateByIdAndFormInfoId(formFieldDO);
    }

    @Override
    public void deleteByIdAndFormInfoId(Integer formFieldId, Integer formInfoId) {
        formFieldMapper.deleteByIdAndFormInfoId(formFieldId, formInfoId);
    }

    @Override
    public void insert(FormFieldDO formFieldDO) {
        FormInfoDO info1 = formInfoMapper.getById(formFieldDO.getFormId());
        if (info1 == null) {
            throw new BackJsonException("不存在该表单");
        }
        formFieldMapper.insert(formFieldDO);
    }

    @Override
    public void setRequired(Integer formFieldId, Integer formInfoId, Boolean isRequired) {
        formFieldMapper.setFieldValueByIdAndFormInfoId(formFieldId, formInfoId, "required", isRequired);
    }

    @Override
    public void setDisplay(Integer formFieldId, Integer formInfoId, Boolean isDisplay) {
        formFieldMapper.setFieldValueByIdAndFormInfoId(formFieldId, formInfoId, "display", isDisplay);
    }

    @Override
    public FormFieldDO getByIdAndFormInfoId(Integer formFieldId, Integer formInfoId) {
        return formFieldMapper.getByIdAndFormInfoId(formFieldId, formInfoId);
    }
}
