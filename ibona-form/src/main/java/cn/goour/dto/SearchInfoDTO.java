package cn.goour.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author 侯坤林
 */
@Data
public class SearchInfoDTO {
    // 查询表单和查询字段共有的查询条件信息
    /**
     * 查询表单字段名称，Null时忽略该条件
     */
    private String name;
    /**
     * 查询表单字段创建时间，开始时间条件，Null时忽略该条件
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time1;
    /**
     * 查询表单字段创建时间，结束时间条件，Null时忽略该条件
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time2;


    // 查询表单字段信息独有的查询条件信息
    /**
     * 查询表单字段是否必填，Null时忽略该条件
     */
    private Boolean required;
    /**
     * 查询表单字段是否显示，Null时忽略该条件
     */
    private Boolean display;
    /**
     * 查询表单字段类型，Null时忽略该条件
     */
    private String type;


    // 查询表单信息独有的查询条件信息
    /**
     * 查询表单是否启用，Null时忽略该条件
     */
    private Boolean enabled;
}
