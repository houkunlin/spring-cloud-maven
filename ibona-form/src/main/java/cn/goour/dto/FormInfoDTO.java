package cn.goour.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * 需要传输给客户的的表单信息
 *
 * @author 侯坤林
 */
@Data
public class FormInfoDTO {
    private Integer id;
    /**
     * 表单名称
     */
    @NotBlank(message = "表单名称不能为空")
    private String name;
    /**
     * 创建时间
     */
    private Date time;
    /**
     * 排序值
     */
    private Integer sort;
    /**
     * 是否启用
     */
    private Boolean enabled;
    /**
     * 备注说明
     */
    private String remark;
}
