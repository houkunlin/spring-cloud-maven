package cn.goour.controller;

import cn.goour.dto.FormInfoDTO;
import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormInfoDO;
import cn.goour.service.FormInfoService;
import cn.goour.utils.FormInfoUtils;
import com.github.pagehelper.PageInfo;
import io.springboot.dto.BackJsonDTO;
import io.springboot.dto.PageInfoDTO;
import io.springboot.exception.BackJsonException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 表单处理控制器
 *
 * @author 侯坤林
 */
@RestController
@RequestMapping
public class FormInfoController {
    @Autowired
    FormInfoService infoService;

    /**
     * 获取表单列表信息
     *
     * @param searchInfoDTO 查询条件
     * @param pageInfoDTO   分页条件
     * @return 返回JSON统一数据格式
     */
    @GetMapping("/list")
    public BackJsonDTO listFormInfo(SearchInfoDTO searchInfoDTO, PageInfoDTO pageInfoDTO) {
        PageInfo<FormInfoDO> pageInfo = infoService.list(searchInfoDTO, pageInfoDTO);
        return FormInfoUtils.createBackJsonByPageInfo(pageInfo);
    }

    /**
     * 获取单个表单信息
     *
     * @param formInfoId 表单ID
     * @return 返回JSON统一数据格式
     */
    @GetMapping(value = "/info", params = {"id"})
    public BackJsonDTO getFormInfoById(@RequestParam("id") Integer formInfoId) {
        if (formInfoId <= 0) {
            throw new BackJsonException("ID必须大于0");
        }
        FormInfoDO info = infoService.getById(formInfoId);
        if (info != null) {
            return new BackJsonDTO(FormInfoUtils.toFormInfoDTO(info));
        } else {
            return new BackJsonDTO(1, "不存在该表单信息");
        }
    }

    /**
     * 添加表单信息
     *
     * @param formInfoDTO 表单信息
     * @param errors      自动注入字段验证错误信息
     * @return 返回JSON统一数据格式
     */
    @PostMapping("/add")
    public BackJsonDTO saveFormInfo(@Valid FormInfoDTO formInfoDTO, Errors errors) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        infoService.insert(FormInfoUtils.toFormInfoDO(formInfoDTO));
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 修改表单信息
     *
     * @param formInfoDTO 表单信息
     * @param errors      自动注入字段验证错误信息
     * @return 返回JSON统一数据格式
     */
    @PostMapping("/update")
    public BackJsonDTO updateFormInfoById(@Valid FormInfoDTO formInfoDTO, Errors errors) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        infoService.updateById(FormInfoUtils.toFormInfoDO(formInfoDTO));
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 删除表单信息
     *
     * @param formInfoId 表单ID
     * @return 返回JSON统一数据格式
     */
    @PostMapping(value = "/delete", params = {"id"})
    public BackJsonDTO deleteFormInfoById(@RequestParam("id") Integer formInfoId) {
        if (formInfoId <= 0) {
            throw new BackJsonException("ID必须大于0");
        }
        infoService.deleteById(formInfoId);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 设置表单是否启用
     *
     * @param formInfoId 表单ID
     * @param isEnabled  是否启用
     * @return 返回JSON统一数据格式
     */
    @PostMapping(value = "/setEnabled", params = {"id", "bool"})
    public BackJsonDTO setFormInfoEnabledById(@RequestParam("id") Integer formInfoId, @RequestParam("bool") Boolean isEnabled) {
        checkParams(formInfoId, isEnabled);
        infoService.setEnabled(formInfoId, isEnabled);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 检查参数是否符合
     *
     * @param id   表单ID
     * @param bool 布尔值，不能为空
     * @throws BackJsonException 如果不符合则抛出异常
     */
    private void checkParams(Integer id, Boolean bool) throws BackJsonException {
        if (id == null || bool == null) {
            throw new BackJsonException("参数不能为空");
        }
        if (id <= 0) {
            throw new BackJsonException("ID必须大于0");
        }
    }
}
