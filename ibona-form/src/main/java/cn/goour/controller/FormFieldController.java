package cn.goour.controller;

import cn.goour.dto.FormFieldDTO;
import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormFieldDO;
import cn.goour.service.FormFieldService;
import cn.goour.utils.FormFieldUtils;
import com.github.pagehelper.PageInfo;
import io.springboot.dto.BackJsonDTO;
import io.springboot.dto.PageInfoDTO;
import io.springboot.exception.BackJsonException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 表单字段信息处理控制器
 *
 * @author 侯坤林
 */
@RestController
@RequestMapping("/{formInfoId}/field")
public class FormFieldController {
    @Autowired
    FormFieldService fieldService;

    /**
     * 获取该表单的字段列表
     *
     * @param searchInfoDTO 查询条件
     * @param pageInfoDTO   分页条件
     * @param formInfoId    表单ID
     * @return 返回统一JSON数据
     */
    @GetMapping("/list")
    public BackJsonDTO listFormField(
            SearchInfoDTO searchInfoDTO,
            PageInfoDTO pageInfoDTO,
            @PathVariable("formInfoId") Integer formInfoId
    ) {
        PageInfo<FormFieldDO> list = fieldService.list(searchInfoDTO, pageInfoDTO, formInfoId);
        return FormFieldUtils.createBackJsonByPageInfo(list);
    }

    /**
     * 获取某个字段的信息
     *
     * @param formFieldId 字段ID
     * @param formInfoId  关联表单id
     * @return 返回统一JSON数据
     */
    @GetMapping(value = "/info", params = {"id"})
    public BackJsonDTO getFormFieldById(@RequestParam("id") Integer formFieldId, @PathVariable("formInfoId") Integer formInfoId) {
        if (formFieldId <= 0) {
            throw new BackJsonException("ID必须大于0");
        }
        FormFieldDO formFieldDO = fieldService.getByIdAndFormInfoId(formFieldId, formInfoId);
        if (formFieldDO != null) {
            return new BackJsonDTO(FormFieldUtils.toFormFieldDTO(formFieldDO));
        } else {
            return new BackJsonDTO(1, "不存在该表单字段信息");
        }
    }

    /**
     * 添加表单字段信息
     *
     * @param formFieldDTO 字段信息
     * @param errors       参数验证错误信息
     * @param formInfoId   表单ID
     * @return 返回统一JSON数据
     */
    @PostMapping("/add")
    public BackJsonDTO saveFormField(@Valid FormFieldDTO formFieldDTO, Errors errors, @PathVariable("formInfoId") Integer formInfoId) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        formFieldDTO.setFormId(formInfoId);
        fieldService.insert(FormFieldUtils.toFormFieldDO(formFieldDTO));
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 修改表单字段信息
     *
     * @param formFieldDTO 字段信息
     * @param errors       参数验证错误信息
     * @param formInfoId   关联表单id
     * @return 返回统一JSON数据
     */
    @PostMapping("/update")
    public BackJsonDTO updateFormFieldById(
            @Valid FormFieldDTO formFieldDTO,
            Errors errors,
            @PathVariable("formInfoId") Integer formInfoId
    ) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        formFieldDTO.setFormId(formInfoId);
        fieldService.updateByIdAndFormInfoId(FormFieldUtils.toFormFieldDO(formFieldDTO));
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 删除字段信息
     *
     * @param formFieldId 字段ID
     * @param formInfoId  关联表单id
     * @return 返回统一JSON数据
     */
    @PostMapping(value = "/delete", params = {"id"})
    public BackJsonDTO deleteFormFieldById(@RequestParam("id") Integer formFieldId, @PathVariable("formInfoId") Integer formInfoId) {
        if (formFieldId <= 0) {
            throw new BackJsonException("ID必须大于0");
        }
        fieldService.deleteByIdAndFormInfoId(formFieldId, formInfoId);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 设置字段是否必填，主要在字段列表界面会用到该借口
     *
     * @param formFieldId 字段ID
     * @param formInfoId  关联表单id
     * @param isRequired  是否必填
     * @return 返回统一JSON数据
     */
    @PostMapping(value = "/setRequired", params = {"id", "bool"})
    public BackJsonDTO setFormFieldRequiredById(
            @RequestParam("id") Integer formFieldId,
            @PathVariable("formInfoId") Integer formInfoId,
            @RequestParam("bool") Boolean isRequired
    ) {
        checkParams(formFieldId, isRequired);
        fieldService.setRequired(formFieldId, formInfoId, isRequired);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 设置字段是否显示，主要在字段列表界面会用到该借口
     *
     * @param formFieldId 字段ID
     * @param formInfoId  关联表单id
     * @param isDisplay   是否显示
     * @return 返回统一JSON数据
     */
    @PostMapping(value = "/setDisplay", params = {"id", "bool"})
    public BackJsonDTO setFormFieldDisplayById(
            @RequestParam("id") Integer formFieldId,
            @PathVariable("formInfoId") Integer formInfoId,
            @RequestParam("bool") Boolean isDisplay
    ) {
        checkParams(formFieldId, isDisplay);
        fieldService.setDisplay(formFieldId, formInfoId, isDisplay);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 检查参数是否错误
     *
     * @param id   字段ID
     * @param bool 布尔值
     * @throws BackJsonException 抛出错误一场
     */
    private void checkParams(Integer id, Boolean bool) throws BackJsonException {
        if (id == null || bool == null) {
            throw new BackJsonException("参数不能为空");
        }
        if (id <= 0) {
            throw new BackJsonException("ID必须大于0");
        }
    }
}
