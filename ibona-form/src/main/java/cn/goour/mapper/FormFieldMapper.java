package cn.goour.mapper;

import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormFieldDO;
import cn.goour.entity.FormInfoDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 表单字段信息 mybatis mapper 接口
 *
 * @author 侯坤林
 */
public interface FormFieldMapper {
    /**
     * 通过字段主键ID和表单主键id删除一条数据
     *
     * @param formFieldId 字段ID
     * @param formInfoId  关联表单id
     */
    void deleteByIdAndFormInfoId(@Param("id") Integer formFieldId, @Param("formInfoId") Integer formInfoId);

    /**
     * 通过字段主键ID删除一条数据
     *
     * @param formFieldId 字段ID
     */
    void deleteById(@Param("id") Integer formFieldId);

    /**
     * 通过关联的表单ID删除多条字段信息
     *
     * @param formInfoId 表单ID
     */
    void deleteByFormId(@Param("id") Integer formInfoId);

    /**
     * 插入一条字段信息
     *
     * @param formFieldDO 字段信息
     */
    void insert(FormFieldDO formFieldDO);

    /**
     * 通过字段id和表单id查询一条字段信息
     *
     * @param formFieldId 字段ID
     * @param formInfoId  关联表单id
     * @return 字段信息
     */
    FormFieldDO getByIdAndFormInfoId(@Param("id") Integer formFieldId, @Param("formInfoId") Integer formInfoId);

    /**
     * 通过字段ID来查找与这个字段关联的表单信息
     *
     * @param formFieldId 字段信息ID
     * @return 表单信息DO
     */
    FormInfoDO getFormInfoByFormFieldId(Integer formFieldId);

    /**
     * 通过字段ID和表单ID来修改一条字段信息，会修改该记录的所有字段
     *
     * @param formFieldDO 字段信息
     */
    void updateByIdAndFormInfoId(FormFieldDO formFieldDO);

    /**
     * 设置数据库的一个字段值
     *
     * @param formFieldId 字段ID
     * @param formInfoId  关联表单信息id
     * @param fieldName   数据库字段名
     * @param value       需要修改的结果值
     */
    void setFieldValueByIdAndFormInfoId(
            @Param("id") Integer formFieldId,
            @Param("formInfoId") Integer formInfoId,
            @Param("field") String fieldName,
            @Param("value") Object value
    );

    /**
     * 通过分页获取列表
     *
     * @param searchInfoDTO 查询条件
     * @param formInfoId    表单ID
     * @return 列表
     */
    List<FormFieldDO> listFormField(@Param("search") SearchInfoDTO searchInfoDTO, @Param("formInfoId") Integer formInfoId);
}