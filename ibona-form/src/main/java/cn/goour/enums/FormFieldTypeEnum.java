package cn.goour.enums;

/**
 * 字段类型枚举值
 *
 * @author 侯坤林
 */
public enum FormFieldTypeEnum {
    /**
     * 文本类型
     */
    TEXT("文本类型"),
    NUMBER("数字类型"),
    TEXT_AREA("长文本类型"),
    DATE("日期类型"),
    DATETIME("日期时间类型"),
    TIME("时间类型"),
    ;

    private String text;

    FormFieldTypeEnum(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
