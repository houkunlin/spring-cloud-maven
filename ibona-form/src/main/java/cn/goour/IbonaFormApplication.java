package cn.goour;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

//@EnableFeignClients

/**
 * 启动类
 *
 * @author 侯坤林
 */
@SpringBootApplication(scanBasePackages = {"cn.goour", "io.springboot"})
@EnableDiscoveryClient
@MapperScan("cn.goour.mapper")
public class IbonaFormApplication {
    public static void main(String[] args) {
        SpringApplication.run(IbonaFormApplication.class, args);
    }
}
