/*
package cn.goour.ibonaform;

import cn.goour.bean.SearchFormInfo;
import cn.goour.entity.FormInfoDO;
import cn.goour.service.FormInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestFormInfoService {

    @Autowired
    FormInfoService infoService;

    @Test
    public void testList() {
        SearchFormInfo searchFormInfo = new SearchFormInfo();
        List<FormInfoDO> list = infoService.list(searchFormInfo);
        System.out.println("====== 获取所有列表 ======");
        System.out.println(list.getClass().getSimpleName());
        System.out.println(list);
    }

    @Test
    public void testAdd() {
        FormInfoDO info = new FormInfoDO();
        info.setName("表单名称");
        info.setTime(new Date());
        info.setSort(1);
        info.setEnabled(true);
        info.setRemark("表单说明");
        infoService.add(info);

        testList();
    }

    @Test
    public void testUpdate() {
        FormInfoDO info = infoService.list().get(0);
        System.out.println(info);
        info.setName(String.format("我在%s更改了名称", format.format(new Date())));
        infoService.update(info);

        testList();
    }

    @Test
    public void testDelete() {
        testList();
        List<FormInfoDO> list = infoService.list();
        infoService.delete(list.get(list.size() - 1).getId());
        testList();
    }

    @Test
    public void testSetEnabled() {
        FormInfoDO info = infoService.list().get(0);
        System.out.println(info);
        info.setEnabled(!info.getEnabled());
        infoService.update(info);

        testList();
    }

    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
*/
