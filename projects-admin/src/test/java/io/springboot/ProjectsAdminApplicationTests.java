package io.springboot;

import io.springboot.dto.ProjectBuildParamsDTO;
import io.springboot.service.ProjectServiceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectsAdminApplicationTests {

    @Autowired
    ProjectServiceService serviceService;

    @Test
    public void testBuildSpringCloudModule() {
        ProjectBuildParamsDTO dto = new ProjectBuildParamsDTO();
        dto.setGitUrl("https://gitee.com/houkunlin/spring-cloud-maven.git");
        dto.setBranch("develop");
        dto.setModule("projects-admin");
        dto.setSkipTest(true);
        serviceService.buildProjectService(dto);
    }

    @Test
    public void testBuildProject(){
        ProjectBuildParamsDTO dto = new ProjectBuildParamsDTO();
        dto.setGitUrl("https://gitee.com/houkunlin/SpringBootMybatis.git");
        dto.setBranch("master");
        dto.setModule(null);
        dto.setSkipTest(true);
        serviceService.buildProjectService(dto);
    }
}
