CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` text,
  `people` varchar(45) NOT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `del` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `lang` varchar(255) NOT NULL,
  `people` varchar(45) NOT NULL,
  `git_url` varchar(255) NOT NULL,
  `add_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL,
  `del` bit(1) NOT NULL,
  `version` int(11) DEFAULT '1',
  `project_id` int(11) NOT NULL,
  `branch` varchar(45) NOT NULL,
  `build` bit(1) DEFAULT NULL,
  `build_out_path` varchar(255) DEFAULT NULL,
  `build_out_message` text,
  `build_result_code` int(11) DEFAULT NULL,
  `build_start_time` datetime DEFAULT NULL,
  `build_end_time` datetime DEFAULT NULL,
  `module` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
