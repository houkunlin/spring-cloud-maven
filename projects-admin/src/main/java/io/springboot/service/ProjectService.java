package io.springboot.service;

import com.github.pagehelper.PageInfo;
import io.springboot.dto.PageInfoDTO;
import io.springboot.entity.ProjectDO;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目信息服务层接口
 *
 * @author 侯坤林
 * @date 18-11-18 上午10:57
 */
public interface ProjectService {
    /**
     * 保存项目信息
     *
     * @param project 项目信息
     */
    void saveProject(ProjectDO project);

    /**
     * 修改项目信息
     *
     * @param project 新的项目信息
     */
    void updateProjectById(ProjectDO project);

    /**
     * 通过项目id获取项目信息
     *
     * @param projectId 项目id
     * @return 项目信息
     */
    ProjectDO getById(Integer projectId);

    /**
     * 通过项目id删除项目信息
     *
     * @param projectId 项目id
     */
    void deleteById(Integer projectId);

    /**
     * 获取项目信息列表
     *
     * @param pageInfoDTO 列表分页条件
     * @param isDel       项目删除状态：true只显示删除，false只显示正常，null全部显示
     * @return 项目列表
     */
    PageInfo<ProjectDO> listProjects(PageInfoDTO pageInfoDTO, Boolean isDel);
}
