package io.springboot.service;

import com.github.pagehelper.PageInfo;
import io.springboot.dto.PageInfoDTO;
import io.springboot.dto.ProjectBuildParamsDTO;
import io.springboot.entity.ProjectServiceDO;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目服务信息服务层接口
 *
 * @author 侯坤林
 * @date 18-11-18 上午11:21
 */
public interface ProjectServiceService {
    /**
     * 保存项目服务信息
     *
     * @param projectServiceDO 项目服务信息
     */
    void saveProject(ProjectServiceDO projectServiceDO);

    /**
     * 修改项目服务信息
     *
     * @param projectServiceDO 新的项目服务信息
     */
    void updateProjectByIdAndProjectId(ProjectServiceDO projectServiceDO);

    /**
     * 通过项目id获取项目服务信息
     *
     * @param projectServiceId 项目服务id
     * @param projectId        项目信息id
     * @return 项目信息
     */
    ProjectServiceDO getByIdAndProjectId(Integer projectServiceId, Integer projectId);

    /**
     * 通过项目id删除项目服务信息
     *
     * @param projectServiceId 项目服务id
     * @param projectId        项目信息id
     */
    void deleteByIdAndProjectId(Integer projectServiceId, Integer projectId);

    /**
     * 获取项目服务信息列表
     *
     * @param pageInfoDTO 列表分页条件
     * @param projectId   项目信息id
     * @param isDel       项目服务删除状态：true只显示删除，false只显示正常，null全部显示
     * @return 项目列表
     */
    PageInfo<ProjectServiceDO> listProjects(PageInfoDTO pageInfoDTO, Integer projectId, Boolean isDel);

    /**
     * 下载源码，自动判断项目类型，然后执行项目构建。
     *
     * @param projectBuildParamsDTO 构建项目信息
     */
    void buildProjectService(ProjectBuildParamsDTO projectBuildParamsDTO);
}
