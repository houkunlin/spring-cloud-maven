package io.springboot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.springboot.dto.PageInfoDTO;
import io.springboot.entity.ProjectDO;
import io.springboot.mapper.ProjectMapper;
import io.springboot.mapper.ProjectServiceMapper;
import io.springboot.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 *
 * @author 侯坤林
 * @date 18-11-18 上午11:25
 */
@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    ProjectMapper projectMapper;
    @Autowired
    ProjectServiceMapper projectServiceMapper;

    @Override
    public void saveProject(ProjectDO project) {
        projectMapper.insert(project);
    }

    @Override
    public void updateProjectById(ProjectDO project) {
        projectMapper.updateById(project);
    }

    @Override
    public ProjectDO getById(Integer projectId) {
        return projectMapper.getById(projectId);
    }

    @Override
    public void deleteById(Integer projectId) {
        projectMapper.deleteById(projectId);
        projectServiceMapper.deleteByProjectId(projectId);
    }

    @Override
    public PageInfo<ProjectDO> listProjects(PageInfoDTO pageInfoDTO, Boolean isDel) {
        return PageHelper.startPage(pageInfoDTO.getPage(), pageInfoDTO.getLimit()).doSelectPageInfo(() -> projectMapper.listProjects(isDel));
    }
}
