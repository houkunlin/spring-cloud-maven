package io.springboot.service.impl;

import io.springboot.dto.ProjectBuildParamsDTO;
import io.springboot.service.BaseBuildProjectService;
import io.springboot.utils.JavaExecShellUtil;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;

/**
 * Maven 项目构建具体实现。
 * Created by 侯坤林(houkunlin@ibona.cn) on 2018/11/20.
 *
 * @author 侯坤林
 * @date 2018/11/20 09:30
 */
@Service
public class MavenBuildProjectServiceImpl extends BaseBuildProjectService {
    @Override
    public JavaExecShellUtil.ShellResult buildProjectService(ProjectBuildParamsDTO projectBuildParamsDTO, File projectDir) {
        ArrayList<String> cmdArr = new ArrayList<>();
        cmdArr.add("mvn package");

        // 模块化项目构建命令
        if (projectBuildParamsDTO.getModule() != null) {
            cmdArr.add(String.format("-pl %s -am", projectBuildParamsDTO.getModule()));

        }

        // 跳过单元测试
        if (projectBuildParamsDTO.getSkipTest()) {
            cmdArr.add("-Dmaven.test.skip=true");
        }
        // 执行构建脚本
        String cmd = String.join(" ", cmdArr);

        logger.debug("执行项目构建命令：" + cmd);

        return JavaExecShellUtil.exec(cmd, projectDir);
    }

    @Override
    public String getOutDir() {
        return "/target";
    }
}
