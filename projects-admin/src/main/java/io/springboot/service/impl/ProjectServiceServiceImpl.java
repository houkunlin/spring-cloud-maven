package io.springboot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.springboot.dto.PageInfoDTO;
import io.springboot.dto.ProjectBuildParamsDTO;
import io.springboot.entity.ProjectServiceDO;
import io.springboot.enums.ProjectTypeEnum;
import io.springboot.mapper.ProjectServiceMapper;
import io.springboot.service.BaseBuildProjectService;
import io.springboot.service.ProjectServiceService;
import io.springboot.utils.JavaExecShellUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 *
 * @author 侯坤林
 * @date 18-11-18 上午11:25
 */
@Service
public class ProjectServiceServiceImpl implements ProjectServiceService {
    @Autowired
    ProjectServiceMapper projectServiceMapper;

    @Autowired
    MavenBuildProjectServiceImpl mavenBuildProjectService;

    @Autowired
    GradleBuildProjectServiceImpl gradleBuildProjectService;

    private Logger logger = LoggerFactory.getLogger(ProjectServiceServiceImpl.class);


    @Override
    public void saveProject(ProjectServiceDO projectServiceDO) {
        projectServiceMapper.insert(projectServiceDO);
    }

    @Override
    public void updateProjectByIdAndProjectId(ProjectServiceDO projectServiceDO) {
        // 需要手动维护项目更新时间
        projectServiceDO.setUpdateTime(new Date());
        projectServiceMapper.updateByIdAndProjectId(projectServiceDO);
    }

    @Override
    public ProjectServiceDO getByIdAndProjectId(Integer projectServiceId, Integer projectId) {
        return projectServiceMapper.getByIdAndProjectId(projectServiceId, projectId);
    }

    @Override
    public void deleteByIdAndProjectId(Integer projectServiceId, Integer projectId) {
        projectServiceMapper.deleteByIdAndProjectId(projectServiceId, projectId);
    }

    @Override
    public PageInfo<ProjectServiceDO> listProjects(PageInfoDTO pageInfoDTO, Integer projectId, Boolean isDel) {
        return PageHelper.startPage(pageInfoDTO.getPage(), pageInfoDTO.getLimit()).doSelectPageInfo(() -> projectServiceMapper.listProjectServices(projectId, isDel));
    }

    /**
     * 下载源码，自动判断项目类型，然后执行项目构建。
     *
     * @param projectBuildParamsDTO 构建项目信息
     */
    @Async
    @Override
    public void buildProjectService(ProjectBuildParamsDTO projectBuildParamsDTO) {
        logger.debug("构建项目配置：" + projectBuildParamsDTO);
        ProjectServiceDO serviceDO = new ProjectServiceDO();
        serviceDO.setId(projectBuildParamsDTO.getProjectServiceId());
        serviceDO.setBuild(true);
        serviceDO.setBuildResultCode(-1);
        serviceDO.setBuildOutPath("");
        serviceDO.setBuildOutMessage("");
        serviceDO.setBuildStartTime(new Date());
        serviceDO.setBuildEndTime(null);
        projectServiceMapper.updateBuildInfoById(serviceDO);

        try {
            serviceDO.setBuild(false);
            // 获取临时工作目录（同时也是项目代码存放目录、项目构建目录）
            File workFile = BaseBuildProjectService.createTempDir("build");
            // 前期准备工作：开始克隆代码和切换分支
            String ready = BaseBuildProjectService.readyProjectCode(projectBuildParamsDTO, workFile);
            if (ready != null) {
                // 操作失败
                logger.debug("前期准备工作失败：" + ready);
///                System.out.println(ready);
                serviceDO.setBuildOutMessage(ready);
                return;
            }

            // 获取项目类型
            ProjectTypeEnum typeEnum = getProjectTypeEnum(projectBuildParamsDTO, workFile);
            // 获取该项目的编译实现
            BaseBuildProjectService buildProjectService = getBuildProjectServiceImpl(typeEnum);

            if (buildProjectService == null) {
                // 无法识别的项目类型
                logger.debug("这是一个无法识别的项目类型（非Maven/Gradle）");
                serviceDO.setBuildOutMessage("这是一个无法识别的项目类型（非Maven/Gradle）");
                return;
            }

            // 开始执行项目构建
            JavaExecShellUtil.ShellResult result = buildProjectService.buildProjectService(projectBuildParamsDTO, workFile);
            logger.debug("构建结果" + result.toString("构建结果"));

            if (result.getCode() == 0) {
                File outFile = buildProjectService.getOutFilePath(workFile, projectBuildParamsDTO.getModule());
                // 移动构建结果文件
                logger.debug("输出文件路径：" + outFile);
                serviceDO.setBuildResultCode(result.getCode());
                serviceDO.setBuildOutMessage(result.toString("项目构建成功"));
                serviceDO.setBuildOutPath(outFile.getAbsolutePath());
            } else {
                // 构建失败
                serviceDO.setBuildOutMessage(result.toString("项目构建失败"));
            }

            // 所有工作完成后删除工作目录
            if (workFile.delete()) {
                logger.debug("删除工作目录：" + workFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
            logger.debug("项目构建失败，java运行时失败：" + e.getLocalizedMessage());
            serviceDO.setBuildOutMessage("项目构建失败，java运行时失败：" + e.getLocalizedMessage());
        } finally {
            serviceDO.setBuildEndTime(new Date());
            logger.debug("构建任务执行完毕：" + projectBuildParamsDTO);
            projectServiceMapper.updateBuildInfoById(serviceDO);
        }
    }

    /**
     * 获取项目类型：maven/gradle。
     * 首先判断是否为自动识别类型，如果是则通过项目代码目录关键文件来判断项目类型，如果手动指定类型则直接返回。
     *
     * @param projectBuildParamsDTO 编译项目信息
     * @param workFile              工作目录
     * @return 项目类型
     */
    private ProjectTypeEnum getProjectTypeEnum(ProjectBuildParamsDTO projectBuildParamsDTO, File workFile) {
        ProjectTypeEnum typeEnum = null;
        if (projectBuildParamsDTO.getType() == null || projectBuildParamsDTO.getType() == ProjectTypeEnum.AUTO) {
            // 自动识别类型
            typeEnum = BaseBuildProjectService.getProjectType(workFile);
            projectBuildParamsDTO.setType(typeEnum);
        } else {
            // 指定项目类型
            typeEnum = projectBuildParamsDTO.getType();
        }
        return typeEnum;
    }

    /**
     * 通过项目类型获得该类型的项目编译具体实现
     *
     * @param typeEnum 项目类型
     * @return 项目具体实现
     */
    private BaseBuildProjectService getBuildProjectServiceImpl(ProjectTypeEnum typeEnum) {
        BaseBuildProjectService buildProjectService = null;
        switch (typeEnum) {
            case MAVEN:
                buildProjectService = mavenBuildProjectService;
                break;
            case GRADLE:
                buildProjectService = gradleBuildProjectService;
                break;
            default:
        }
        return buildProjectService;
    }
}
