package io.springboot.service.impl;

import io.springboot.dto.ProjectBuildParamsDTO;
import io.springboot.service.BaseBuildProjectService;
import io.springboot.utils.JavaExecShellUtil;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 2018/11/20.
 *
 * @author 侯坤林
 * @date 2018/11/20 11:18
 */
@Service
public class GradleBuildProjectServiceImpl extends BaseBuildProjectService {
    @Override
    public JavaExecShellUtil.ShellResult buildProjectService(ProjectBuildParamsDTO projectBuildParamsDTO, File projectDir) {
        return null;
    }

    @Override
    public String getOutDir() {
        return "/out/libs";
    }
}
