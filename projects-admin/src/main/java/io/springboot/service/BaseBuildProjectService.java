package io.springboot.service;

import io.springboot.dto.ProjectBuildParamsDTO;
import io.springboot.enums.ProjectTypeEnum;
import io.springboot.utils.JavaExecShellUtil;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * 构建项目功能接口。
 * Created by 侯坤林(houkunlin@ibona.cn) on 2018/11/20.
 *
 * @author 侯坤林
 * @date 2018/11/20 09:26
 */
public abstract class BaseBuildProjectService {
    protected static Logger logger = LoggerFactory.getLogger(BaseBuildProjectService.class);

    /**
     * 构建项目，开始项目构建。
     *
     * @param projectBuildParamsDTO 构建项目信息
     * @param workDir               项目路径
     * @return 返回构建结果
     */
    public abstract JavaExecShellUtil.ShellResult buildProjectService(ProjectBuildParamsDTO projectBuildParamsDTO, File workDir);

    /**
     * 获取项目构建结果文件夹名称
     *
     * @return 文件夹名称
     */
    public abstract String getOutDir();

    /**
     * 获取的构建结果输出文件
     *
     * @param workDir 项目构建路径
     * @param module  构建模块名称，如果没有则传null
     * @return 构建结果输出文件
     */
    public File getOutFilePath(File workDir, String module) {
        // 构建成功，构建编译输出目录
        File out = new File(workDir, (module != null ? module : "") + getOutDir());
        logger.debug("项目构建输出地址：" + out);

        File[] files = Objects.requireNonNull(out.listFiles());
        for (File file : files) {
            if (file.getName().endsWith(".jar") || file.getName().endsWith(".war")) {
                logger.debug("项目构建输出文件地址：" + file);
                return file;
            }
        }
        return null;
    }

    /**
     * 获取的构建结果输出文件
     *
     * @param workDir 项目构建路径
     * @return 构建结果输出文件
     */
    public File getOutFilePath(File workDir) {
        return getOutFilePath(workDir, null);
    }

    /**
     * 构建项目，执行源码下载。
     *
     * @param projectBuildParamsDTO 构建项目信息
     */
    public JavaExecShellUtil.ShellResult buildProjectService(ProjectBuildParamsDTO projectBuildParamsDTO) {
        // 获取临时目录
        JavaExecShellUtil.ShellResult result = new JavaExecShellUtil.ShellResult();
        try {
            // 获取临时工作目录（同时也是项目代码存放目录、项目构建目录）
            File workFile = BaseBuildProjectService.createTempDir("build");

            // 前期准备工作：开始克隆代码和切换分支
            String ready = BaseBuildProjectService.readyProjectCode(projectBuildParamsDTO, workFile);
            if (ready != null) {
                // 操作失败
                logger.debug("前期准备工作失败：" + ready);
                result.setCode(-1);
                result.setOut(ready);
                return result;
            }
            return buildProjectService(projectBuildParamsDTO, workFile);
        } catch (IOException e) {
            e.printStackTrace();
            result.setCode(-1);
            result.setOut(e.getLocalizedMessage());
        }
        return result;
    }

    /**
     * 克隆项目地址
     *
     * @param projectBuildParamsDTO 编译项目信息
     * @param workDir               工作路径
     * @return 返回执行结果
     */
    private static JavaExecShellUtil.ShellResult gitCloneProjectCode(ProjectBuildParamsDTO projectBuildParamsDTO, File workDir) {
        // 下载项目源码，并保存到当前workDir路径
        String cmd = String.format("git clone %s .", projectBuildParamsDTO.getGitUrl());
        logger.debug("执行代码克隆到本地：" + cmd);
        return JavaExecShellUtil.exec(cmd, workDir);
    }

    /**
     * 切换项目分支
     *
     * @param projectBuildParamsDTO 编译项目信息
     * @param workDir               项目保持路径
     * @return 执行结构
     */
    private static JavaExecShellUtil.ShellResult gitCheckoutBranch(ProjectBuildParamsDTO projectBuildParamsDTO, File workDir) {
        // 切换项目分支
        String cmd = String.format("git checkout %s", projectBuildParamsDTO.getBranch());
        logger.debug("切换项目分支：" + cmd);
        return JavaExecShellUtil.exec(cmd, workDir);
    }

    /**
     * 获取项目的类型：maven、gradle
     *
     * @param workDir 项目代码保存位置
     * @return 项目类型
     */
    public static ProjectTypeEnum getProjectType(File workDir) {
        ProjectTypeEnum typeEnum = null;
        File[] files = Objects.requireNonNull(workDir.listFiles());
        for (File file : files) {
            if ("pom.xml".equals(file.getName().toLowerCase())) {
                typeEnum = ProjectTypeEnum.MAVEN;
            } else if ("build.gradle".equals(file.getName().toLowerCase())) {
                typeEnum = ProjectTypeEnum.GRADLE;
            }
        }
        logger.debug("获取项目类型：" + typeEnum);
        return typeEnum;
    }

    /**
     * 编译项目前期代码准备工作
     *
     * @param projectBuildParamsDTO 项目信息
     * @param workDir               工作目录（代码保存位置）
     * @return 执行结果
     */
    public static String readyProjectCode(ProjectBuildParamsDTO projectBuildParamsDTO, File workDir) {
        JavaExecShellUtil.ShellResult result = new JavaExecShellUtil.ShellResult();
        // 开始克隆代码
        JavaExecShellUtil.ShellResult clone = BaseBuildProjectService.gitCloneProjectCode(projectBuildParamsDTO, workDir);
        if (clone.getCode() != 0) {
            // 克隆代码失败
            logger.debug("克隆代码失败");
            return result.toString("克隆代码失败");
        }

        // 切换项目分支
        JavaExecShellUtil.ShellResult checkout = BaseBuildProjectService.gitCheckoutBranch(projectBuildParamsDTO, workDir);
        if (checkout.getCode() != 0) {
            // 切换项目分支失败
            logger.debug("切换项目分支失败");
            return checkout.toString("切换项目分支失败");
        }
        return null;
    }

    public static File createTempDir(String prefix) throws IOException {
        String tempPath = System.getProperty("java.io.tmpdir") + File.separator + "autoBuild/" + prefix + RandomStringUtils.randomAlphabetic(5) + System.nanoTime();
        File file = new File(tempPath);
        if (file.mkdirs()) {
            logger.debug("临时工作目录：" + file);
            return file;
        }
        logger.debug("创建临时目录失败");
        throw new IOException("创建临时目录失败");
    }
}
