package io.springboot.mapper;

import io.springboot.entity.ProjectDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目信息数据库操作接口
 *
 * @author 侯坤林
 * @date 18-11-18 上午10:16
 */
public interface ProjectMapper {
    /**
     * 添加项目信息
     *
     * @param project 项目信息
     */
    void insert(ProjectDO project);

    /**
     * 通过主键id修改项目信息
     *
     * @param project 新的项目信息
     */
    void updateById(ProjectDO project);

    /**
     * 通过项目主键id获取项目信息
     *
     * @param projectId 主键id
     * @return 项目信息
     */
    ProjectDO getById(@Param("projectId") Integer projectId);

    /**
     * 通过项目主键id删除项目信息
     *
     * @param projectId 主键id
     */
    void deleteById(@Param("projectId") Integer projectId);

    /**
     * 获取所有的项目信息
     *
     * @param isDel 项目删除状态：true只显示删除，false只显示正常，null全部显示
     * @return 项目列表
     */
    List<ProjectDO> listProjects(@Param("del") Boolean isDel);
}
