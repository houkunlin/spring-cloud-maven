package io.springboot.mapper;

import io.springboot.entity.ProjectServiceDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目服务信息数据库操作接口
 *
 * @author 侯坤林
 * @date 18-11-18 上午10:38
 */
public interface ProjectServiceMapper {
    /**
     * 添加项目服务
     *
     * @param projectService 项目服务
     */
    void insert(ProjectServiceDO projectService);

    /**
     * 通过主键id修改项目服务
     *
     * @param projectService 新的项目服务
     */
    void updateByIdAndProjectId(ProjectServiceDO projectService);

    /**
     * 修改构建信息
     *
     * @param projectService 新的构建信息
     */
    void updateBuildInfoById(ProjectServiceDO projectService);

    /**
     * 通过主键id获取项目服务信息
     *
     * @param projectServiceId 项目服务主键id
     * @param projectId        项目信息id
     * @return 项目服务信息
     */
    ProjectServiceDO getByIdAndProjectId(@Param("projectServiceId") Integer projectServiceId, @Param("projectId") Integer projectId);

    /**
     * 通过项目服务主键id删除项目服务信息
     *
     * @param projectServiceId 项目服务主键id
     * @param projectId        项目信息id
     */
    void deleteByIdAndProjectId(@Param("projectServiceId") Integer projectServiceId, @Param("projectId") Integer projectId);

    /**
     * 通过关联项目id删除服务信息
     *
     * @param projectId 项目id
     */
    void deleteByProjectId(@Param("projectId") Integer projectId);


    /**
     * 查找项目下的项目服务信息
     *
     * @param projectId 项目信息id
     * @param isDel     项目服务删除状态：true只显示删除，false只显示正常，null全部显示
     * @return 项目服务列表
     */
    List<ProjectServiceDO> listProjectServices(@Param("projectId") Integer projectId, @Param("del") Boolean isDel);
}
