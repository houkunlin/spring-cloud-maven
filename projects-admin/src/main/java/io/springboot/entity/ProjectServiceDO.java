package io.springboot.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目服务信息实体类
 *
 * @author 侯坤林
 * @date 18-11-18 上午10:14
 */
@Data
public class ProjectServiceDO {
    private Integer id;
    @NotBlank(message = "项目服务名称不能为空")
    private String title;
    private String text;
    @NotBlank(message = "项目服务语言不能为空")
    private String lang;
    @NotBlank(message = "项目服务负责人不能为空")
    private String people;
    @NotBlank(message = "项目服务git地址不能为空")
    private String gitUrl;
    @NotBlank(message = "项目服务git分支不能为空")
    private String branch;
    /**
     * 多模块项目，项目在该git仓库下的模块名
     */
    private String module;
    private Date addTime;
    private Date updateTime;
    private Boolean del;
    private Integer version;
    /**
     * 项目是否正在编译构建
     */
    private Boolean build;
    /**
     * 项目编译构建结果，0成功，非0失败
     */
    private Integer buildResultCode;
    /**
     * 项目构建输出文件路径
     */
    private String buildOutPath;
    /**
     * 项目构建过程中输出信息
     */
    private String buildOutMessage;
    /**
     * 构建开始时间
     */
    private Date buildStartTime;
    /**
     * 构建结束时间
     */
    private Date buildEndTime;
    private Integer projectId;

    public ProjectServiceDO() {
        this.del = false;
    }
}
