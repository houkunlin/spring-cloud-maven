package io.springboot.entity;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目信息实体类
 *
 * @author 侯坤林
 * @date 18-11-18 上午10:08
 */
@Data
public class ProjectDO {
    private Integer id;
    @NotBlank(message = "项目名称不能为空")
    private String title;
    private String text;
    @NotBlank(message = "项目负责人不能为空")
    private String people;
    private Date addTime;
    private Date updateTime;
    private Boolean del;

    public ProjectDO() {
        this.del = false;
    }
}
