package io.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author 侯坤林
 */
@SpringBootApplication(scanBasePackages = {"io.springboot"})
@EnableDiscoveryClient
@EnableAsync
@MapperScan("io.springboot.mapper")
public class ProjectsAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectsAdminApplication.class, args);
    }
}
