package io.springboot.controller;

import io.springboot.dto.BackJsonDTO;
import io.springboot.dto.PageInfoDTO;
import io.springboot.dto.ProjectBuildParamsDTO;
import io.springboot.entity.ProjectServiceDO;
import io.springboot.exception.BackJsonException;
import io.springboot.service.ProjectServiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目服务信息controller层对外提供服务接口
 *
 * @author 侯坤林
 * @date 18-11-18 上午11:31
 */
@RestController
@RequestMapping("/{projectId}/service")
public class ProjectServiceController {
    @Autowired
    ProjectServiceService projectServiceService;

    private Logger logger = LoggerFactory.getLogger(ProjectServiceController.class);

    /**
     * 获取项目服务信息列表
     *
     * @param pageInfoDTO 分页信息
     * @param projectId   项目信息id
     * @return 列表结果
     */
    @GetMapping("/list")
    public BackJsonDTO list(PageInfoDTO pageInfoDTO, @PathVariable("projectId") Integer projectId) {
        return new BackJsonDTO(projectServiceService.listProjects(pageInfoDTO, projectId, false));
    }

    /**
     * 获取一个项目服务信息
     *
     * @param projectServiceId 项目服务id
     * @return 返回结果
     */
    @GetMapping("/info")
    public BackJsonDTO info(@RequestParam("id") Integer projectServiceId, @PathVariable("projectId") Integer projectId) {
        return new BackJsonDTO(projectServiceService.getByIdAndProjectId(projectServiceId, projectId));
    }

    /**
     * 添加项目服务信息
     *
     * @param projectServiceDO 项目服务信息
     * @return 添加结果
     */
    @PostMapping("/add")
    public BackJsonDTO add(@Valid ProjectServiceDO projectServiceDO, Errors errors, @PathVariable("projectId") Integer projectId) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        projectServiceDO.setProjectId(projectId);
        projectServiceService.saveProject(projectServiceDO);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 修改项目服务信息
     *
     * @param projectServiceDO 新的项目服务信息
     * @return 修改结果
     */
    @PostMapping("/update")
    public BackJsonDTO update(@Valid ProjectServiceDO projectServiceDO, Errors errors, @PathVariable("projectId") Integer projectId) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        projectServiceDO.setProjectId(projectId);
        projectServiceService.updateProjectByIdAndProjectId(projectServiceDO);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 删除项目服务信息
     *
     * @param projectServiceId 项目服务信息id
     * @return 删除结果
     */
    @PostMapping("/delete")
    public BackJsonDTO update(@RequestParam("id") Integer projectServiceId, @PathVariable("projectId") Integer projectId) {
        projectServiceService.deleteByIdAndProjectId(projectServiceId, projectId);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    @PostMapping("/build")
    public BackJsonDTO buildProject(@Valid ProjectBuildParamsDTO buildParamsDTO, Errors errors, @PathVariable("projectId") Integer projectId) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        ProjectServiceDO projectService = projectServiceService.getByIdAndProjectId(buildParamsDTO.getProjectServiceId(), projectId);
        if (projectService == null) {
            return new BackJsonDTO(1, "没有找到该项目服务");
        }
        if (projectService.getBuild() != null && projectService.getBuild()) {
            return new BackJsonDTO(1, "当前项目正在构建，请不要重复构建");
        }
        buildParamsDTO.setGitUrl(projectService.getGitUrl());
        buildParamsDTO.setBranch(projectService.getBranch());
        buildParamsDTO.setModule(projectService.getModule());
        projectServiceService.buildProjectService(buildParamsDTO);

        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }
}
