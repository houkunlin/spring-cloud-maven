package io.springboot.controller;

import io.springboot.dto.BackJsonDTO;
import io.springboot.dto.PageInfoDTO;
import io.springboot.entity.ProjectDO;
import io.springboot.exception.BackJsonException;
import io.springboot.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-18.
 * 项目信息controller对外提供服务接口
 *
 * @author 侯坤林
 * @date 18-11-18 上午11:30
 */
@RestController
@RequestMapping
public class ProjectController {
    @Autowired
    ProjectService projectService;

    /**
     * 获取项目信息列表
     *
     * @param pageInfoDTO 分页信息
     * @return 列表结果
     */
    @GetMapping("/list")
    public BackJsonDTO list(PageInfoDTO pageInfoDTO) {
        return new BackJsonDTO(projectService.listProjects(pageInfoDTO, false));
    }

    /**
     * 获取一个项目信息
     *
     * @param projectId 项目id
     * @return 返回结果
     */
    @GetMapping("/info")
    public BackJsonDTO info(@RequestParam("id") Integer projectId) {
        return new BackJsonDTO(projectService.getById(projectId));
    }

    /**
     * 添加项目信息
     *
     * @param projectDO 项目信息
     * @return 添加结果
     */
    @PostMapping("/add")
    public BackJsonDTO add(@Valid ProjectDO projectDO, Errors errors) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        projectService.saveProject(projectDO);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 修改项目信息
     *
     * @param projectDO 新的项目信息
     * @return 修改结果
     */
    @PostMapping("/update")
    public BackJsonDTO update(@Valid ProjectDO projectDO, Errors errors) {
        if (errors.hasErrors()) {
            BackJsonException.throwErrorBackJson(errors);
        }
        projectService.updateProjectById(projectDO);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }

    /**
     * 删除项目信息
     *
     * @param projectId 项目信息id
     * @return 删除结果
     */
    @PostMapping("/delete")
    public BackJsonDTO update(@RequestParam("id") Integer projectId) {
        projectService.deleteById(projectId);
        return BackJsonDTO.DEFAULT_SUCCESS_MSG;
    }
}
