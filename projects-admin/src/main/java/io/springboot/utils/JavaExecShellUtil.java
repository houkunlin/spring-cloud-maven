package io.springboot.utils;

import lombok.Data;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 18-11-19.
 *
 * @author 侯坤林
 * @date 18-11-19 下午10:03
 */
public class JavaExecShellUtil {

    @Data
    public static class ShellResult {
        private Integer code;
        private String out;
        private String err;

        public String toString(String tag) {
            return "\n\n===============" +
                    "\n===============" +
                    tag +
                    "\n===============\n" +
                    "执行返回代码：" + code +
                    "\n正常输出内容：\n" +
                    out +
                    "\n===============" +
                    "\n错误输出内容：\n" +
                    err +
                    "\n===============\n\n\n";
        }
    }

    public static ShellResult exec(String cmd, File path) {
        Runtime runtime = Runtime.getRuntime();
        ShellResult result = new ShellResult();
        StringBuilder out = new StringBuilder();
        StringBuilder err = new StringBuilder();

        try {
            Process process = runtime.exec(cmd, null, path);
            BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stderrReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line;
            while ((line = stdoutReader.readLine()) != null) {
                out.append(line);
                out.append("\n");
            }
            while ((line = stderrReader.readLine()) != null) {
                err.append(line);
                err.append("\n");
            }
            int exitVal = process.waitFor();
            result.setCode(exitVal);
        } catch (IOException | InterruptedException e) {
            result.setCode(-1);
            err.append("Java运行异常错误：").append(e.getMessage());
            e.printStackTrace();
        }
        result.setOut(out.toString());
        result.setErr(err.toString());
        return result;
    }

    public static ShellResult exec(String[] cmd, File path) {
        return null;
    }
}
