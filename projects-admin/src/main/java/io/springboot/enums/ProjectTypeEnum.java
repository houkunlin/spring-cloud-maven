package io.springboot.enums;

/**
 * Created by 侯坤林(houkunlin@ibona.cn) on 2018/11/20.
 *
 * @author 侯坤林
 * @date 2018/11/20 09:18
 */
public enum ProjectTypeEnum {
    /**
     * 自动识别项目类型
     */
    AUTO,
    /**
     * Maven项目
     */
    MAVEN,
    /**
     * gradle项目
     */
    GRADLE,
    ;

}
