package io.springboot.dto;

import io.springboot.enums.ProjectTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 项目构建参数信息。
 * Created by 侯坤林(houkunlin@ibona.cn) on 2018/11/20.
 *
 * @author 侯坤林
 * @date 2018/11/20 09:16
 */
@Data
public class ProjectBuildParamsDTO {
    /**
     * 需要构建的项目服务ID
     */
    @NotNull(message = "项目服务ID不能为空")
    private Integer projectServiceId;
    /**
     * 项目仓库地址
     */
    private String gitUrl;
    /**
     * 项目所在分支
     */
    private String branch;
    /**
     * 项目在这个仓库分支下的某个模块。用于构建多模块项目
     */
    private String module;
    /**
     * 构建时是否跳过单元测试
     */
    private Boolean skipTest;
    /**
     * 项目类型：maven、gradle，auto自动识别
     */
    private ProjectTypeEnum type;

    public ProjectBuildParamsDTO() {
        this.skipTest = true;
        this.type = ProjectTypeEnum.AUTO;
    }
}
