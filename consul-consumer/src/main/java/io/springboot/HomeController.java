package io.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping
public class HomeController {
    @Autowired
    DiscoveryClient discoveryClient;

    @Autowired
    LoadBalancerClient loadBalancerClient;

    @Autowired
    RestTemplate restTemplate;

    /**
     * 获取注册中心所有的在注册的微服务名称
     *
     * @return 结果
     */
    @RequestMapping("/dc1")
    public String home() {
        String services = "Services: " + discoveryClient.getServices();
        System.out.println(services);
        return services;
    }

    /**
     * 通过某个微服务名称获取它的访问路径
     *
     * @param name 微服务名称
     * @return 结果
     */
    @RequestMapping("/dc2")
    public String serviceUrl(String name) {
        List<ServiceInstance> list = discoveryClient.getInstances(name);
        StringBuffer stringBuffer = new StringBuffer();
        list.forEach(item -> {
            stringBuffer.append(item.getUri());
            stringBuffer.append("<br>");
        });
        return stringBuffer.toString();
    }

    /**
     * 通过某个微服务名称获取该微服务的信息，然后拼接URL访问该微服务。
     * 也可以直接访问网关，通过网关来访问其他的微服务。
     *
     * @param name 微服务名词
     * @param uri  访问的URI
     * @return 访问结果
     */
    @GetMapping("/")
    public String dc(String name, String uri) {
        ServiceInstance serviceInstance = loadBalancerClient.choose(name);
        System.out.println(serviceInstance);
        String url = "http://" + serviceInstance.getHost() + ":" + serviceInstance.getPort() + uri;
        System.out.println(url);
        return restTemplate.getForObject(url, String.class);
    }
}
