package io.springboot.dto;

import com.github.pagehelper.PageInfo;
import lombok.Data;

/**
 * 统一JSON数据返回对象
 *
 * @author 侯坤林
 */
@Data
public class BackJsonDTO {
    /**
     * Controller层操作成功返回默认的成功JSON数据
     */
    public static final BackJsonDTO DEFAULT_SUCCESS_MSG = new BackJsonDTO();
    /**
     * 状态码
     */
    private Integer code;
    /**
     * 状态信息
     */
    private String message;
    /**
     * 返回数据
     */
    private Object data;
    /**
     * 返回列表时表示数据总数
     */
    private Long count;
    /**
     * 返回列表时表示每页数据条数
     */
    private Integer limit;
    /**
     * 当前页数
     */
    private Integer page;

    public BackJsonDTO() {
        this.code = 0;
        this.message = "success";
    }

    /**
     * 构造一个简单的状态数据
     *
     * @param code    状态码
     * @param message 状态信息
     */
    public BackJsonDTO(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 构造一个有对象返回的数据
     *
     * @param data 返回数据
     */
    public BackJsonDTO(Object data) {
        this();
        this.data = data;
    }

    /**
     * 显示mybatis分页信息
     *
     * @param pageInfo 列表数据，含分页信息
     */
    public BackJsonDTO(PageInfo<?> pageInfo) {
        this();
        this.data = pageInfo.getList();
        this.count = pageInfo.getTotal();
        this.limit = pageInfo.getPageSize();
        this.page = pageInfo.getPageNum();
    }
}
