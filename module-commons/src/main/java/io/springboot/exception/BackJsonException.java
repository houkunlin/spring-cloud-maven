package io.springboot.exception;

import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;

/**
 * 统一手动抛出异常。
 *
 * @author 侯坤林
 */
public class BackJsonException extends RuntimeException {


    public BackJsonException() {
    }

    public BackJsonException(String message) {
        super(message);
    }

    public BackJsonException(String message, Throwable cause) {
        super(message, cause);
    }

    public BackJsonException(Throwable cause) {
        super(cause);
    }

    public BackJsonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * controller层参数校验失败时使用该方法抛出异常
     *
     * @param errors 参数校验错误信息
     */
    public static void throwErrorBackJson(Errors errors) {
        List<String> arr = new ArrayList<>();
        errors.getAllErrors().forEach(error -> {
            String msg = error.getDefaultMessage();
            arr.add(msg == null || msg.length() > 20 ? "参数校验失败，请检查参数是否正确" : msg);
        });

        throw new BackJsonException(arr.toString());
    }
}
