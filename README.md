## 跑一遍网上的SpringBootCloud教程
### 各模块说明
- consul-consumer
    对注册consul注册中心的一个简单调用：获取所有在线微服务、获取微服务地址、访问微服务
- ibona-form
    志亮给的一个原型里面的表单管理功能
- module-commons
    一个公共模块，在ibona-form和projects-admin会依赖它，包括统一json格式和统一异常处理
- projects-admin
    坐我对面的国强给徐功伟的一个小功能任务，自己也拿来练练手。主要包括项目和该项目服务的管理，还有项目自动编译功能（部署功能暂未添加）
- service-feign
    【跑一遍网上的教程】网上的关于feign调用的教程，一个简单的使用
- service-zuul
    【跑一遍网上的教程】zuul网关的简单使用
    
### 分支说明
开发分支开发完成的功能会合并到主分支。
- master 主分支
- develop 开发分支